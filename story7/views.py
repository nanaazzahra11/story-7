from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required

def story7(request):
    return render(request, 'story7.html')

def story8(request):
    return render(request, 'story8.html')

def indexView(request):
    return render(request, 'index.html')

@login_required
def dashboardView(request):
    return redirect('story7:story7')

def login(request):
    return render(request, "login.html")

def registerView(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login/')
    else:
        form = UserCreationForm() 
    return render(request, 'register.html', {'form': form})
