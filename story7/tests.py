from django.urls import resolve
from django.test import TestCase, Client
from .views import *
from .models import *

class UnitTestStory8(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, story7)

    def test_using_to_do_list_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'story7.html')

    def test_url_book_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_using_book_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)

    def test_using_book_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_story9_is_exist(self):
      response = Client().get('/login/')
      self.assertEqual(response.status_code, 200)

    def test_story9_404_error(self):
      response = Client().get('/page/')
      self.assertEqual(response.status_code, 404)
    
    def test_login_user_exist(self):
        response = self.client.get('/login/')

        self.assertContains(response, 'Masuk')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')

