from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'story7'

urlpatterns = [
    path('', views.story7, name='story7'),
    path('story8/', views.story8, name='story8'),
    path('index/', views.indexView, name='index'),
    path('login/', LoginView.as_view(), name = 'login_url'),
    path('register/',views.registerView, name='register_url'),
    path('dashboard/', views.dashboardView, name="dashboard_url"),
    path('logout/',LogoutView.as_view(next_page="story7:index"), name="logout_url"),
]
